package com.wecyou.wecu;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.content.Intent;
import com.bluecityspa.wecucore.WEcuCore;
import com.bluecityspa.wecucore.activities.VideoActivity;

/**
* This class WEcu a string called from JavaScript.
*/
public class WEcu extends CordovaPlugin {

    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        switch (action) {
            case "isCam":
                isCam(args.getString(0), args.getInt(1), args.getInt(2), callbackContext);
                break;
            case "setCam":
                setCam(args.getString(0), callbackContext);
                break;
            case "setLicense":
                setLicense(args.getString(0), callbackContext);
                break;
            case "createSession":
                String guest = args.getString(0);
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        createSession(guest, callbackContext);
                    }
                });
                break;
            case "joinSession":
                String sessionId = args.getString(0);
                cordova.getActivity().runOnUiThread(new Runnable() {
                    public void run() {
                        joinSession(sessionId, callbackContext);
                    }
                });
                break;
            default:
                return false;
        }

        return true;
    }

    public void isCam(String address, int port, int timeout, CallbackContext callbackContext) {
        if (address != null && address.length() > 0) {
            boolean found = WEcuCore.isCam(address, port, timeout);
            callbackContext.success("" + found);            
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    public void setCam(String address, CallbackContext callbackContext) {
        if (address != null) {
            WEcuCore.setCam(address);
            callbackContext.success();            
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    public void setLicense(String key, CallbackContext callbackContext) {
        if (key != null) {
            WEcuCore.setLicense(key);
            callbackContext.success();            
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }

    public void createSession(String guest, CallbackContext callbackContext) {
        WEcuCore.mGuest = guest == "null" ? null : guest;
        WEcuCore.mSessionID = "";
        Context context = cordova.getActivity().getApplicationContext();
        Intent intent = new Intent(context, VideoActivity.class);
        cordova.getActivity().startActivity(intent);
        callbackContext.success(guest);            
    }

    public void joinSession(String sessionId, CallbackContext callbackContext) {
        if (sessionId != null) {
            WEcuCore.mGuest = "";
            WEcuCore.mSessionID = sessionId;
            Context context = cordova.getActivity().getApplicationContext();
            Intent intent = new Intent(context, VideoActivity.class);
            cordova.getActivity().startActivity(intent);
            callbackContext.success(sessionId);            
        } else {
            callbackContext.error("Expected one non-empty string argument.");
        }
    }
}