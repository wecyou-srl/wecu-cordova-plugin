//
//  WEcuCore.h
//  WEcuCore
//
//  Created by Gerardo Grisolini on 20/03/18.
//  Copyright © 2018 Gerardo Grisolini. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WEcuCore.
FOUNDATION_EXPORT double WEcuCoreVersionNumber;

//! Project version string for WEcuCore.
FOUNDATION_EXPORT const unsigned char WEcuCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WEcuCore/PublicHeader.h>
