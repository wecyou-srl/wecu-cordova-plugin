import WEcuCore

@objc(WEcu) class WEcu : CDVPlugin {

  @objc(isCam:)
  func isCam(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    if let address = command.arguments[0] as? String,
        let port = command.arguments[1] as? Int32,
        let timeout = command.arguments[2] as? Int32 {

      let isCam = WEcuCore.isCam(address: address, port: port, timeout: timeout)

      pluginResult = CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: isCam
      )
    }

    self.commandDelegate!.send(
      pluginResult,
      callbackId: command.callbackId
    )
  }

  @objc(setCam:)
  func setCam(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    if let address = command.arguments[0] as? String {
      WEcuCore.setCam(address: address)

      pluginResult = CDVPluginResult(
        status: CDVCommandStatus_OK
      )
    }

    self.commandDelegate!.send(
      pluginResult,
      callbackId: command.callbackId
    )
  }

  @objc(setLicense:)
  func setLicense(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    if let key = command.arguments[0] as? String {
      WEcuCore.setLicense(key: key)

      pluginResult = CDVPluginResult(
        status: CDVCommandStatus_OK
      )
    }

    self.commandDelegate!.send(
      pluginResult,
      callbackId: command.callbackId
    )
  }
  
  @objc(createSession:)
  func createSession(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    let guest = command.arguments[0] as? String   
    let wecuController = WEcuCore.create(guest: guest)
    self.viewController?.present(
      wecuController,
      animated: true,
      completion: nil
    )

    self.commandDelegate!.send(
      CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: guest
      ),
      callbackId: command.callbackId
    )
  }

  @objc(joinSession:)
  func joinSession(command: CDVInvokedUrlCommand) {

    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    if let sessionId = command.arguments[0] as? String {    
      let wecuController = WEcuCore.join(sessionId: sessionId)
      self.viewController?.present(
        wecuController,
        animated: true,
        completion: nil
      )
        
      pluginResult = CDVPluginResult(
        status: CDVCommandStatus_OK,
        messageAs: sessionId
      )
    }

    self.commandDelegate!.send(
      pluginResult,
      callbackId: command.callbackId
    )
  }
}
