# wecu-cordova-plugin


Let�s generate a new Cordova app, add the bridging header generation plugin and our plugin to it, then test that everything�s working. To bootstrap a test app, use the following commands:
```
cordova create testapp com.bluecityspa.wecu TestApp
cd testapp
```

## Installation platforms and plugin
```
cordova plugin add cordova-plugin-add-swift-support --save
cordova platform add ios
cordova platform add android
cordova plugin add https://bitbucket.org/wecyou-srl/wecu-cordova-plugin.git
```

## iOS configuration

#### Swift
Add this in your AppDelegate
```
import WEcuCore

let wecucore = WEcuCore()

func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
    return WEcuCore.application(open: url)
}

func applicationWillEnterForeground(_ application: UIApplication) {
    WEcuCore.willEnterForeground()
}
```

#### Objective-C
```
#import "WEcuCore/WEcuCore.h"

WEcuCore *wecucore;

- (BOOL)application:(UIApplication*)application didFinishLaunchingWithOptions:(NSDictionary*)launchOptions {
    wecucore = [[WEcuCore alloc] init];
    ...
    return [super application:application didFinishLaunchingWithOptions:launchOptions];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    return [WEcuCore applicationWithOpen: url];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [WEcuCore willEnterForeground];
}
```

#### Share extension
```
Download and configure this extension in your project for sharing:
<a href="https://bitbucket.org/bluecitysrl/wecu-cordova-plugin/src/master/src/iOS/WEcuShare.zip">WEcuShare</a>
```

## Android configuration
Enable Firebase Messaging in your app 
```
FirebaseMessaging.getInstance().getToken()
    .addOnCompleteListener(new OnCompleteListener<String>() {
        @Override
        public void onComplete(@NonNull Task<String> task) {
            if (!task.isSuccessful()) {
                Log.w(TAG, "Fetching FCM registration token failed", task.getException());
                return;
            }

            // Get new FCM registration token
            String refreshedToken = task.getResult();
            WEcuCore.saveNotificationToken(refreshedToken);
        }
    });
```

## Build and deploy

### iOS
```
cordova build --device ios
```

### Android
```
cordova build android
```

### Example

```
var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
        
        
        // Set a custom camera
        if (wecu.isCam('192.168.1.10', 5001, 1000,
            function(msg) { alert(msg); },
            function(err) { alert(err); })
        ) {
            wecu.setCam(
                'Camera 1', '192.168.1.10', 5001, 1280, 720, 15, 1700000,
                function() { alert('Camera successfully registered'); },
                function(err) { alert(err); }
            );
        }

        // Set a license key
        wecu.setLicense(
            '1234-1234-1234-1234',
            function() { alert('License successfully registered'); },
            function(err) { alert(err); }
        );

        // Create new session
        wecu.createSession(
            'info@domain.com',
            function(msg) { console.log(msg) },
            function(err) {  alert(err); }
        );

        // Join an active session
        wecu.joinSession(
            '12345678',
            function(msg) { console.log(msg) },
            function(err) { alert(err); }
        );
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();
```
