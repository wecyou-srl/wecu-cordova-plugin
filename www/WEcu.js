var exec = require('cordova/exec');

exports.isCam = function (arg0, arg1, arg2, success, error) {
    exec(success, error, 'WEcu', 'isCam', [arg0, arg1, arg2]);
};

exports.setCam = function (arg0, success, error) {
    exec(success, error, 'WEcu', 'setCam', [arg0]);
};

exports.setLicense = function (arg0, success, error) {
    exec(success, error, 'WEcu', 'setLicense', [arg0]);
};

exports.createSession = function (arg0, success, error) {
    exec(success, error, 'WEcu', 'createSession', [arg0]);
};

exports.joinSession = function (arg0, success, error) {
    exec(success, error, 'WEcu', 'joinSession', [arg0]);
};
